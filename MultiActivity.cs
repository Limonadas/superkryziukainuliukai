﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Essentials;

namespace THEGAMEBOIIII
{
    [Activity(Label = "MultiActivity")]
    public class MultiActivity : Activity
    {
        List<TableLayout> boards;
        bool cross = false;
        TableLayout tbl1;
        TableLayout tbl2;
        TableLayout tbl3;
        TableLayout tbl4;
        TableLayout tbl5;
        TableLayout tbl6;
        TableLayout tbl7;
        TableLayout tbl8;
        TableLayout tbl9;
        TextView alert;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.multiLayout);
            FillEverthing();
            if (Connectivity.NetworkAccess == NetworkAccess.Internet)
                alert.Alpha = 0;
            else
                alert.Alpha = 1;
            Connectivity.ConnectivityChanged += Connectivity_ConnectivityChanged;
        }

        private void Connectivity_ConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            if (e.NetworkAccess == NetworkAccess.Internet)
                alert.Alpha = 0;
            else
                alert.Alpha = 1;
        }
        private void FillEverthing()
        {
            alert = FindViewById(Resource.Id.alertMulti) as TextView;
            tbl1 = FindViewById(Resource.Id.tbl1) as TableLayout;
            tbl2 = FindViewById(Resource.Id.tbl2) as TableLayout;
            tbl3 = FindViewById(Resource.Id.tbl3) as TableLayout;
            tbl4 = FindViewById(Resource.Id.tbl4) as TableLayout;
            tbl5 = FindViewById(Resource.Id.tbl5) as TableLayout;
            tbl6 = FindViewById(Resource.Id.tbl6) as TableLayout;
            tbl7 = FindViewById(Resource.Id.tbl7) as TableLayout;
            tbl8 = FindViewById(Resource.Id.tbl8) as TableLayout;
            tbl9 = FindViewById(Resource.Id.tbl9) as TableLayout;
            boards = new List<TableLayout>
            {
                tbl1,
                tbl2,
                tbl3,
                tbl4,
                tbl5,
                tbl6,
                tbl7,
                tbl8,
                tbl9
            };
            foreach (var board in boards)
            {
                for (int i = 0, j = board.ChildCount; i < j; i++)
                {
                    View view = board.GetChildAt(i);
                    TableRow row = (TableRow)view;
                    for (int h = 0, k = row.ChildCount; h < k; h++)
                    {
                        View nview = row.GetChildAt(h);
                        ImageButton btn = (ImageButton)nview;
                        btn.Click += new EventHandler(Test);
                    }
                }
            }
        }
        private void Test(object sender, EventArgs e)
        {
            var btn = (ImageButton)sender;
            if (btn.Tag.ToString() == "blank")
            {
                if (cross)
                {
                    btn.SetImageResource(Resource.Drawable.cross);
                    cross = false;
                    btn.Tag = "cross";
                }
                else
                {
                    btn.SetImageResource(Resource.Drawable.circle);
                    cross = true;
                    btn.Tag = "circle";
                }
                btn.Enabled = false;
            }
        }
    }
}