﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using System;
using Android.Graphics.Drawables;
using System.Collections.Generic;
using Android.Views;
using Android.Content;
using Xamarin.Essentials;

namespace THEGAMEBOIIII
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        Button btnSingle;
        Button btnMulti;
        Button btnOptions;
        TextView alert;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.startLayout);

            btnSingle = FindViewById(Resource.Id.singleplayer) as Button;
            btnMulti = FindViewById(Resource.Id.multiplayer) as Button;
            btnOptions = FindViewById(Resource.Id.options) as Button;
            alert = FindViewById(Resource.Id.alert) as TextView;
            btnSingle.Click += BtnSingle_Click;
            btnMulti.Click += BtnMulti_Click;
            btnOptions.Click += BtnOptions_Click;
            if (Connectivity.NetworkAccess == NetworkAccess.Internet)
                alert.Alpha = 0;
            else
                alert.Alpha = 1;
            Connectivity.ConnectivityChanged += Connectivity_ConnectivityChanged;
        }

        private void Connectivity_ConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            if (e.NetworkAccess == NetworkAccess.Internet)
                alert.Alpha = 0;
            else
                alert.Alpha = 1;
        }

        private void BtnOptions_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(OptionsActivity));
            StartActivity(intent);
        }

        private void BtnMulti_Click(object sender, EventArgs e)
        {
            if (Connectivity.NetworkAccess == NetworkAccess.Internet)
            {
                Intent intent = new Intent(this, typeof(MultiActivity));
                StartActivity(intent);
            }
            else
            {
                Toast t = Toast.MakeText(this, "Please connect to the internet", ToastLength.Short);
                t.Show();
            }
        }

        private void BtnSingle_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(SingleActivity));
            StartActivity(intent);
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}